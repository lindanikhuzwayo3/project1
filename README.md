# project1

The pi hat is a UPS that serves as backup power when there's a power outage or cut thus to safely shutdown the raspberry pi zero

This microHAT will keep your Pi alive for long enough to shut it
down properly (or if your battery is big enough to survive the blackout).
This hat is used as follows:

the hat is attached to the pi zero through gpio pins this when the power cuts the UPS kicks in.
this safely shuts down the pi zero .   
